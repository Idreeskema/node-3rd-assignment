const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const mongoose = require("mongoose");
const dbURI = `mongodb+srv://root:12345@cluster0.w93q1.mongodb.net/job-portal?retryWrites=true&w=majority`;
const app = express();
const PORT = 8080;

app.set("view engine", "pug");
app.set("views", "views");

const sequelize = require("./util/database");
const adminRoutes = require("./routes/admin");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use("/admin", adminRoutes);

app.get("/about-us", (req, res, next) => {
  res.render("about-us", {
    pageTitle: "About Us",
    path: "about-us",
  });
});
/* sequelize.sync({ force: true }).then((result) => {
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}); */

/* mongoose
  .connect(
    "mongodb+srv://root:12345@cluster0.w93q1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  )
  .then((result) => {
    User.findOne().then((user) => {
      if (!user) {
        const user = new User({
          name: "Max",
          email: "max@test.com",
          cart: {
            items: [],
          },
        });
        user.save();
      }
    });
    app.listen(3000);
  })
  .catch((err) => {
    console.log(err);
  }); */
(async () => {
  try {
    await mongoose.connect(dbURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    await sequelize.sync();

    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  } catch (err) {
    console.log("error: " + err);
  }
})();
